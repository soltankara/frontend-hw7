import React from 'react';
import {shallow} from 'enzyme';
import {AppComponent as App} from '../../src/containers/App';
import GameButtons from '../../src/containers/GameButtonsContainer';

describe('AppComponent', () => {
  it('renders successfully', () => {
    const app = shallow(<App connected={true} />);
    expect(app).not.to.be.empty;
  });

  it('does not render GameButtons when not connected', () => {
    const app = shallow(<App connected={false} />);
    expect(app).not.to.have.descendants(GameButtons);
  });

  it('does renders GameButtons when connected', () => {
    const app = shallow(<App connected={true} />);
    expect(app).to.have.descendants(GameButtons);
  });
});
