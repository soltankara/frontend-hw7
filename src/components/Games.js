import React from 'react';
import PropTypes from 'prop-types';
import HangmanGame from '../components/Hangman/Game';
import RPSGame from '../components/RPS/Game';

const Games = (props) => {
    const gameID = props.match.params.gameId;
    const game = props.games.map((game) => {
        const onGameGuess = (guess) => {
            props.onGuess(game.id, guess);
        };
        if (game.id === gameID) {
            if (game.type === 'hangman') {
                return <HangmanGame key={game.id} onGuess={onGameGuess} {...game} />;
            } else {
                return <RPSGame key={game.id} onGuess={onGameGuess} {...game} />;
            }
        }
    });
    const gameComponents = game ? game : 'Game Not Found';
    return (
        <div>
            {gameComponents}
        </div>
    );
};

Games.propTypes = {
    games: PropTypes.array.isRequired,
    onGuess: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
};
export default Games;
