import React from 'react';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom';

const FinishedGames = ({games}) => {
    const gameComponents = games.map((game, index) => {
        const type = game.type;
        const status = game.status;
        if (status === 'finished') {
            return (
                <ul key={index}>
                    <li><Link to={'/games/'+game.id}>Game Type - {type}; Game Status - {status}</Link></li>
                </ul>
            );
        }
    });
    return (
        <div>
            {gameComponents}
        </div>
    );
};

FinishedGames.propTypes = {
    games: PropTypes.array.isRequired,
};

export default FinishedGames;
