import React from 'react';
import PropTypes from 'prop-types';
import {Route} from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';
import Games from './GamesContainer';
import OngoingGames from './OngoingGamesContainer';
import GameButtons from './GameButtonsContainer';
import Header from '../components/Header';
import OnlinePlayers from './OnlinePlayersContainer';
import ConnectionState from './ConnectionStateContainer';
import {connect} from 'react-redux';
import URLMapping from './URLMapping';
import FinishedGames from './FinishedGamesContainer';

export const AppComponent = ({connected, history}) => {
    let area = null;
    if (connected) {
       area = <URLMapping/>;
    }
    return (
        <ConnectedRouter history={history}>
            <div className="app">
                <Header />
                <ConnectionState />
                    {area}
                    <Route path="/createGame" component={GameButtons} />
                    <Route path="/players" component={OnlinePlayers} />
                    <Route path="/ongoingGames" component={OngoingGames} />
                    <Route path="/finishedGames" component={FinishedGames} />
                    <Route path="/games/:gameId" component={Games} />
            </div>
        </ConnectedRouter>
    );
};

AppComponent.propTypes = {
    connected: PropTypes.bool.isRequired,
    history: PropTypes.object.isRequired
};

const mapStateToProps = ({connection}) => ({
    connected: (connection.connectionState === 'connection_accepted'),
});

const mapDispatchToProps = undefined;

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
