import React from 'react';
import {Link} from 'react-router-dom';

const URLMapping = () => {
    return (
        <ul>
            <li><Link to="/createGame">Create Game</Link></li>
            <li><Link to="/players">Online Players</Link></li>
            <li><Link to="/ongoingGames">Ongoing Games</Link></li>
            <li><Link to="/finishedGames">Finished Games</Link></li>
        </ul>
    );
};
export default URLMapping;
