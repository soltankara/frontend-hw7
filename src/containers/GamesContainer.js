import {connect} from 'react-redux';
import {gameGuessRequested} from '../actions';
import Games from '../components/Games';

const mapStateToProps = ({games}) => {
  return {
      games: Object.values(games.games).sort((id1, id2) => id1 - id2)
  };
};

const mapDispatchToProps = (dispatch) => ({
  onGuess: (gameId, guess) => dispatch(gameGuessRequested({gameId, guess}))
});

export default connect(mapStateToProps, mapDispatchToProps)(Games);
