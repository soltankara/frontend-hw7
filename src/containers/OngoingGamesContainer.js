import {connect} from 'react-redux';
import OngoingGames from '../components/OngoingGames';

const mapStateToProps = ({games}) => ({
    games: Object.values(games.games).sort((id1, id2) => id1 - id2)
});

const mapDispatchToProps = undefined;

export default connect(mapStateToProps, mapDispatchToProps)(OngoingGames);
